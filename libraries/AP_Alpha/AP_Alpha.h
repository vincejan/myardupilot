/// -*- tab-width: 4; Mode: C++; c-basic-offset: 4; indent-tabs-mode: nil -*-
#pragma once

#define ALPHA_ENABLED_DEFAULT 		 0
#define ALPHA_REF1_DEFAULT         -10
#define ALPHA_REF2_DEFAULT          10
#define ALPHA_V1_DEFAULT             0
#define ALPHA_V2_DEFAULT             3.3
#define ALPHA_FILT_C_DEFAULT 		 0.2

#include <AP_HAL/AP_HAL.h>
#include <AP_Common/AP_Common.h>
#include <AP_Param/AP_Param.h>

class AP_Alpha
{
public:
    int16_t product_id;                         /// product id

    //int16_t alpha;                      ///< angle of attack
    uint32_t last_update;               ///< micros() time of last update
    bool healthy;                               ///< true if last read OK

    /// Constructor
    ///
    AP_Alpha();

    bool  enabled();
    float read();
    float get_alpha();
    float get_alpha_smooth();
    float get_voltage();
    void init();

    static const struct AP_Param::GroupInfo var_info[];
protected:

    float _alpha;
    float _alpha_smooth;
    float _voltage;

    AP_Int8  _enabled;
    AP_Float _a1;
    AP_Float _a2;
    AP_Float _v1;
    AP_Float _v2;
    AP_Float _filter_coeff;
    uint8_t _pin;
    AP_HAL::AnalogSource *alpha_adc;


};

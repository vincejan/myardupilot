#include "AP_Alpha.h"

extern const AP_HAL::HAL& hal;

const AP_Param::GroupInfo AP_Alpha::var_info[] = {


	// @Param: ALPHA_ENABLED
	// @DisplayName: Alpha enabled
	// @Description: Angle of attack sensor enabled
	// @User: Advanced
	AP_GROUPINFO("ENABLED", 0, AP_Alpha, _enabled, ALPHA_ENABLED_DEFAULT),

    // @Param: ALPHA_A1
    // @DisplayName: Alpha reference 1
    // @Description: Angle of attack reference 1 in centi-degrees
    // @Range: -18000 18000
    // @User: Advanced
    AP_GROUPINFO("A1", 1, AP_Alpha, _a1, ALPHA_REF1_DEFAULT),

	// @Param: ALPHA_A2
	// @DisplayName: Alpha reference 2
	// @Description: Angle of attack reference 2 in centi-degrees
	// @Range: -18000 18000
	// @User: Advanced
	AP_GROUPINFO("A2", 2, AP_Alpha, _a2, ALPHA_REF2_DEFAULT),

	// @Param: ALPHA_V1
	// @DisplayName: Alpha voltage 1
	// @Description: Angle of attack voltage 1
	// @Range: 0 3.3
	// @User: Advanced
	AP_GROUPINFO("V1", 3, AP_Alpha, _v1, ALPHA_V1_DEFAULT),

	// @Param: ALPHA_V2
	// @DisplayName: Alpha voltage 2
	// @Description: Angle of attack voltage 2
	// @Range: 0 3.3
	// @User: Advanced
	AP_GROUPINFO("V2", 4, AP_Alpha, _v2, ALPHA_V2_DEFAULT),

	// @Param: ALPHA_FILTER
	// @DisplayName: Alpha filter coeff
	// @Description: Angle of attack filter coeff
	// @Range: 0 1
	// @User: Advanced
	AP_GROUPINFO("FILTER_C", 5, AP_Alpha, _filter_coeff, ALPHA_FILT_C_DEFAULT),

    AP_GROUPEND
};



AP_Alpha::AP_Alpha()
		:_alpha(0)
		, _pin(14)
{
	AP_Param::setup_object_defaults(this, var_info);
};

void AP_Alpha::init(){
	alpha_adc = hal.analogin->channel(_pin);
	_enabled = true;
};

float AP_Alpha::read(){

	float a = (_a2-_a1)/(_v2-_v1);
	float b = _a1-a*_v1;

	_voltage = alpha_adc->voltage_latest();
	_alpha = a*_voltage+b;

	_alpha_smooth = _alpha_smooth * (1.0-_filter_coeff) + _alpha * _filter_coeff;

	return _alpha;
};

bool AP_Alpha::enabled(){
	return (bool)_enabled;
}

float AP_Alpha::get_alpha_smooth(){
	return _alpha_smooth;
};

float AP_Alpha::get_alpha(){
	return _alpha;
};

float AP_Alpha::get_voltage(){
	return _voltage;
};
